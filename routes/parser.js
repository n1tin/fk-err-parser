const express = require('express');
const router = express.Router();

router.post('/', (req, res) => {
  let results = '';
  const request = req.body;
  if (request && request.data) {
    const errors = request.data.split('\n');
    let matches = [];
    const details = errors.map(error => {
      matches = error.match(/Error while editing FSN: (.+) in camelot, Reason: \{(.+)\,*$/);
      return JSON.parse(`{"fsn": "${matches[1]}", ${matches[2]}`);
    });
    results = getDetails(details);
  }
  res.send(results);
});

router.get('/', (req, res) => {
  res.send('Success !');
});

const getDetails = (errorData) => {
  const fsnData = [];
  let attrs = [];
  errorData.forEach(data => {
    data.errors && data.errors.forEach(error => {
      if (error.affectedAttributes['']) {
        attrs.push(error.affectedAttributes[''][0].attributeIdentifier.attributeName);
      } else if (error.affectedAttributes['dependency']) {
        attrs.push(error.affectedAttributes['dependency'][0].attributeIdentifier.attributeName);
        attrs.push(error.affectedAttributes['dependant'][0].attributeIdentifier.attributeName);
      }
    });
    fsnData.push(`${data.fsn} ${attrs.join(',')}`);
    attrs = [];
  });
  
  return fsnData;
}

module.exports = router;
