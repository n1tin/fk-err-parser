const express = require('express');
const serverless = require('serverless-http');
const bodyParser = require('body-parser');

const app = express();
const router = express.Router();

const getDetails = (errorData) => {
    const fsnData = [];
    let attrs = [];
    errorData.forEach(data => {
        data.errors && data.errors.forEach(error => {
            if (error.affectedAttributes['']) {
                attrs.push(error.affectedAttributes[''][0].attributeIdentifier.attributeName);
            } else if (error.affectedAttributes['dependency']) {
                attrs.push(error.affectedAttributes['dependency'][0].attributeIdentifier.attributeName);
                attrs.push(error.affectedAttributes['dependant'][0].attributeIdentifier.attributeName);
            }
        });
        fsnData.push(`${data.fsn} ${attrs.join(',')}`);
        attrs = [];
    });

    return fsnData;
}

router.get('/', (req, res) => res.json({ route: req.originalUrl }));
router.post('/parse', (req, res) => {
    let results = '';
    const request = req.body;
    if (request && request.data) {
        const errors = request.data.split('\n');
        let matches = [];
        const details = errors.map(error => {
            matches = error.match(/Error while editing FSN: (.+) in camelot, Reason: \{(.+)\,*$/);
            console.log(error, matches);
            return JSON.parse(`{"fsn": "${matches[1]}", ${matches[2]}`);
        });
        results = getDetails(details);
    }
    res.send(results);
});

app.use(bodyParser.json({ limit: '200mb' }));
app.use(bodyParser.urlencoded({ limit: '200mb', extended: true }));

app.use('/.netlify/functions/api', router);  // path must route to lambda
app.use('/', (req, res) => res.send('Hello !'));

module.exports = app;
module.exports.handler = serverless(app);
