const submitForm = (event) => {
    event.preventDefault();
    const inputElem = document.getElementById('error-data');
    const resultElem = document.getElementById('result');
    const data = inputElem.value;
    fetch('/.netlify/functions/api/parse', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json;charset=utf-8' },
        body: JSON.stringify({ data })
    })
    .then(response => response.json())
    .then((results) => {
        const details = results && results.join('\n');
        resultElem.value = details;
    });
}

const clearInput = () => {
    console.log('clear !');
}

const clearResults = () => {
    console.log('clear results!');
}